# ansible template

This is an ansible template. You are supposed to copy and modify it to your needs.

It tries to collect the common operations that you may need when starting a project.

It also tries to be self-explanatory, so that if you never used ansible you
can still find here a decent overview.

You can find all the resources that you need at https://docs.ansible.com .
There are both good reference guides and full documentation.

Install ansible with
```
sudo pip3 install ansible
```

Upgrade regularly with
```
sudo pip3 install ansible -U
```

## basic playbook

This is the first example of playbook that you encounter.

We need some definitions here:

* host: a remote machine that you want to orchestrate with ansible
* task: defines a state that you want a host to be in
* role: is a collection of tasks
* playbook: maps a series of tasks (or a series of roles) to a host (or host group).

The idea is installing ansible on your laptop and using it to manage a series
of remote machines. 
Remember that you only need a ssh connection to the remote machine in order to
use ansible. You do not need to install any orchestration manager to the 
remote machine.
In this template we use the laptop itself as a remote machine, you do not 
need to setup a local ssh connection.

The playbook is defined in [000-basic.yml](). You can chose the name that you want for this file.

Be careful: if there is any syntax error in one of the yaml files, ansible
crashes badly without giving any proper error message.

In this example, the playbook maps the roles `basic` and `bash` to the host `laptophostname`.
The `user` variable should be changed from `username` to the true user of your laptop.

When launching the playbook, you have to specify the inventory file where the properties of the host that you specified are defined. In this example the hosts are defined in [inventory/hosts.yml](). You can change the name of the directory `inventory` and the file `hosts.yml`.

Ansible automatically loads the variables in the file [group_vars/{{ host }}.yml]() next to the inventory file that you defined (you _can not_ change the name of the directory `group_vars`), in this case it loads [inventory/group_vars/laptophostname.yml]().

### basic role

The tasks that are collected in the role `basic`, which is defined in the directory `roles/basic`, are an overview of how to install packages. You _can not_ change the name of the directory `roles`, while the name of the role called in the playbook file should match the name of the directory inside `roles`.

Ansible, by default, looks for tasks in the file `tasks/main.yml` inside the role directory. In this case it is [roles/basic/tasks/main.yml]().

A task is based on an ansible module. In this example the only module used is `apt`. Browse https://docs.ansible.com/ansible/latest/modules/apt_module.html for more informations.

### dotfiles role

The `copy` module looks for files in the `files` directory inside the role directory, in this case it is [inventory/hosts.yml](). The files are copied without any change. In this example the source file is placed in a subdirectory of the `files` directory.

The `template` is similar to `copy`, but allows for easy personalization of the file. It look for template files in `templates` directory, in this case [roles/dotfiles/templates](). Whenever a string is embraced by `{{ }}` (double curly braces) it is substituted by a variable. Variables are defined or in the playbook file (`user` variable is defined here), or in the inventory group vars (`group` var is defined here), or in the `defaults/main.yml` file inside the role directory, in this example [roles/dotfiles/defaults/main.yml](). 

In this example we want to configure the name and the email field of the gitconfig before copying it (you can find the double curly braces inside the template source file [roles/dotfiles/templates/gitconfig]()), and the variables are defined in [roles/dotfiles/defaults/main.yml]().

The role ends with an example of how to setup a cronjob that archives the directory `/home/{{ user }}/source` to the file `/home/{{ user }}/backup/source.tar.gz`.
You can check that the cronjob has been added running as you user
```
crontab -e
```
 
## network playbook

This a simple playbook that changes the firewalls of the target host, in this case the laptop.

Select the firewall policy and the interface that you use to connect to internet in [roles/iptables/defaults/main.yml](). That variable is used in [roles/iptables/tasks/main.yml]() for selecting the task collection that you desire. 

You can check you current firewall configuration with
```
sudo iptables -S
```
or
```
sudo iptables -L
```

Overview:
* empty: this is the default firewall configuration
* basic: 
  1.  drops all forwarded packets (your laptop is not a router)
  2.  accept all output
  3.  accept input only if 
      1.  are pings (icmp protocol) 
      2.  are established connections
      3.  drop otherwise
* pubnet: This is a more aggressive version of the basic example, mainly opening only some ports. 